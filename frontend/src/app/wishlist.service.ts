import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  constructor(private httpClient: HttpClient) { }

  getWishlistByUserId(userId: number) {
    return this.httpClient.get('http://localhost:8085/getWishlist/' + userId);
  }

  addToWishList(flatId:any, userId:any){
    return this.httpClient.post('http://localhost:8085/addToWishlist', { "wishlistId": 1,"user": { "id": userId }, "flat" : { "flatId": flatId }});
  }

  checkWishlist(userId:any,flatId:any){
    return this.httpClient.get('http://localhost:8085/checkWishlist/'+ userId + '/'+ flatId )
  }
  deleteWishlist(userId:any,flatId:any){
    return this.httpClient.delete('http://localhost:8085/deleteWishlist/'+ userId + '/'+ flatId )

  }
}
