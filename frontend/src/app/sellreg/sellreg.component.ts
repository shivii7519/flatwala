import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MyListingService } from '../my-listing.service';


@Component({
  selector: 'app-sellreg',
  templateUrl: './sellreg.component.html',
  styleUrls: ['./sellreg.component.css']
})
export class SellregComponent implements OnInit {
  flatName: any;
  city: any;
  ownerType: any;
  ownerId: any;
  bHK: any;
  rent: any;
  furnishedStatus: any;
  floorNo: any;
  ownerName: any;
  ownerPhone: any;
  ownerMail: any;
  object: any;
  bathroom: any;
  user:any;
  constructor(private listingService: MyListingService, private service: AuthService, private toastr: ToastrService, private router: Router) {
    this.object = { ownerType: '', ownerName: '', ownerPhone: '', ownerMail: '', flatName: '', city: '', locality: '', bHK: '', rent: '', floorNo: '', furnishedStatus: '', bathroom: '' };
    this.user= JSON.parse(localStorage.getItem('user') || "");
  }

  ngOnInit(): void {

  }

  form = new FormGroup({
    ownerType: new FormControl(''),
    ownerName: new FormControl(''),
    ownerPhone: new FormControl(''),
    ownerMail: new FormControl(''),
    flatName: new FormControl(''),
    city: new FormControl(''),
    locality: new FormControl(''),
    bHK: new FormControl(''),
    rent: new FormControl(''),
    floorNo: new FormControl(''),
    furnishedStatus: new FormControl(''),
    bathroom: new FormControl(''),
    maplink: new FormControl(''),
  });

  submit() {
    this.object = this.form.value;
    console.log(this.object);
    this.service.registerFlat(this.object).subscribe(async (result: any) => {
      this.toastr.success('Flat registered successfully', 'Success!!!')

      console.log("flatId", result);
      this.listingService.addToMyListing(this.user.id, result.flatId).subscribe((data: any) => {
      console.log("uploaded:", data); 
      })

      function delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
      await delay(2000);

      this.router.navigate(["home"])
    }, (error: any) => {
      console.log(error.message);
    });
  }
}
