import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyListingService {

  constructor(private httpClient: HttpClient) { }

  addToMyListing(userId:any, flatId:any){
    return this.httpClient.post('http://localhost:8085/addToMyListing', {
      "listingId": 1,
      "user": {"id": userId},
      "flat": {"flatId": flatId}
  });
  }

  getListedFlats(id:any){
    return this.httpClient.get('http://localhost:8085/getMyListing/'+id);
  }

  deleteMyListingById(userId: any, flatId: any) {
    return this.httpClient.delete('http://localhost:8085/deleteMyListing/' + userId + '/' + flatId);
  }
}
