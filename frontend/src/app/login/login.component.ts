import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = '';
  password: string = '';
  user: any;
  loggedIn: any;

  constructor(private router: Router, private auth: AuthService, private toaster: ToastrService) { }

  ngOnInit() {
    localStorage.clear();
  }

  async login() {
    if (this.email == '') {
      this.toaster.error('Please enter email', 'Error');
      return;
    }

    if (this.password == '') {
      this.toaster.error('Please enter password', 'Error');
      return;
    }

    this.auth.login(this.email, this.password);
    this.email = '';
    this.password = '';
  }

  signWithGoogle() {
    this.auth.googleSignIn().then(data => {
      localStorage.setItem('photoURL', JSON.stringify(data.user?.photoURL));
      this.auth.getUserByEmail(data.user?.email || "").subscribe((res: any) => {
        if (res) {
          localStorage.setItem('user', JSON.stringify(res));
          this.router.navigate(['../home']);
        }
        else {
          this.auth.registerOnlyInDatabase(data.user?.displayName, data.user?.email);
        }
      })
    });
  }
}
