import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { GoogleAuthProvider } from 'firebase/auth';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private fireauth: AngularFireAuth, private router: Router, private httpClient: HttpClient, private toaster: ToastrService) { }

  login(email: string, password: string) {
    this.fireauth.signInWithEmailAndPassword(email, password).then((res) => {
      this.getUserByEmail(email).subscribe(data => {
        if (data != null && data != "") {
          localStorage.setItem('user', JSON.stringify(data));
          this.router.navigate(['/home']);
          this.toaster.success('Log In Success', 'Success');
        }
      })
    }, err => {
      this.toaster.error('Invalid username or password', 'Error');
    })
  }

  register(name: string, email: string, password: string, phoneNumber: string): any {
    this.fireauth.createUserWithEmailAndPassword(email, password).then((res) => {
      console.log('Registered into Firebase: ' + JSON.stringify(res));
      this.sendVerificationEmail(res.user);
    }, err => {
      alert(err.message)
      
    });

    return this.httpClient.post('http://localhost:8085/registerUser', {
      "name": name,
      "email": email,
      "phone": phoneNumber
    });
  }

  registerOnlyInDatabase(name: any, email: any) {
    return this.httpClient.post('http://localhost:8085/registerUser', {
      "name": name,
      "email": email
    }).subscribe(data => {
      console.log("registered in db", data);
      localStorage.setItem('user', JSON.stringify(data));
      this.router.navigate(['/home']);
    });
  }

  getUserByEmail(email: string) {
    return this.httpClient.get('http://localhost:8085/getUser/' + email);
  }

  logout() {
    this.fireauth.signOut().then(() => {
      localStorage.clear();
      this.toaster.success('Logout Success!', 'Success');
      this.router.navigate(['login']);
    }, err => {
      alert(err.message)
    })
  }

  forgetpassword(email: string) {
    this.fireauth.sendPasswordResetEmail(email).then((res) => {
      this.router.navigate(['/verify']);
    }, err => {
      alert('Something went wrong');
    })
  }

  sendVerificationEmail(user: any) {
    this.fireauth.currentUser.then(u => u?.sendEmailVerification())
      .then(() => {
        this.router.navigate(['/verify']);
      }, (err: any) => {
        alert('Something Went Wrong. Not able to send mail to registered Email.');
      })
  }

  googleSignIn() {
    return this.fireauth.signInWithPopup(new GoogleAuthProvider);
  }

  registerFlat(object: any): any {
    return this.httpClient.post('http://localhost:8085/registerFlat', object);
  }

}
