import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
// @Injectable({
//   providedIn: 'flatwala'
// })
export class RentserviceService {

  constructor(private httpClient: HttpClient) { }

  getAllFlats(city:String, bhk:String): any {
    return this.httpClient.get('http://localhost:8085/getFlatsByCity/'+city+'/'+bhk);
  }
  getOwnerById(id:any ){
    console.log("service id", id);
    return this.httpClient.get('http://localhost:8085/getOwnerbyid/'+id);
  }
  getFlatByID(id: any){
    return this.httpClient.get('http://localhost:8085/getflatById/'+id);
  }
}
