import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RentserviceService } from '../rentservice.service';
import { WishlistService } from '../wishlist.service';

declare var jQuery: any;
@Component({
  selector: 'app-rentpage',
  templateUrl: './rentpage.component.html',
  styleUrls: ['./rentpage.component.css']
})
export class RentpageComponent implements OnInit {
  apartments: any[];
  owner: any;
  selectedCity = '';
  selectedBHKs = '';
  user: any;

  constructor(private service: RentserviceService, private router: Router, private wishService: WishlistService, private toaster: ToastrService) {
    this.selectedCity = localStorage.getItem('selectedCity') || "";
    this.selectedBHKs = localStorage.getItem('selectedBHK') || "";
    this.user = JSON.parse(localStorage.getItem('user') || "");
    this.apartments=[];
  };

  ngOnInit(): void {
    this.service.getAllFlats(this.selectedCity, this.selectedBHKs).subscribe((data: any) => { this.apartments = data })
  }

  ownerDetails(flat: any) {
    this.owner = flat;
    jQuery('#ownerModal').modal('show');
  }

  addToWishList(id: any) {
    this.wishService.checkWishlist(this.user.id, id).subscribe((data: any) => {
      if (data.length == 0) {
        this.wishService.addToWishList(id, this.user.id).subscribe((data: any) => {
          console.log("flat added to wishlist");
          this.toaster.success('Flat added to wishlist', 'Success');
        })
      }
      else {
        console.log("flat is already registered");
        this.toaster.warning('Flat already added to wishlist', 'Warning');
      }
    })
  }

  getFlatDetails(id: any) {
    this.router.navigate(['../flatDetails']);
    localStorage.setItem('selectedFlat', id);
  }
}
