import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-flat',
  templateUrl: './search-flat.component.html',
  styleUrls: ['./search-flat.component.css']
})
export class SearchFlatComponent {
  availableCities = ['Mumbai', 'Bengaluru', 'Pune', 'Hyderabad', 'Kolkata', 'Chennai', 'Delhi'];
  availableBHKs = ['1', '2', '3', '4', '5'];

  selectedCity = '';
  selectedBHKs = '';
  constructor(private router:Router){

  }

  search() {
    localStorage.setItem('selectedCity', this.selectedCity);
    localStorage.setItem('selectedBHK', this.selectedBHKs);
    this.router.navigate(['../availableFlats']);
    console.log('Selected city is ' + this.selectedCity);
    console.log('Selected BHK is ' + this.selectedBHKs);
  }
}
