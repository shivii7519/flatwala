import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { AuthService } from '../auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

declare var jQuery: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {
  name: any;
  email: string = '';
  password: string = '';
  otp!: string;
  verify: any;
  phoneNumber: any;
  passwordPtrn = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,16}$';

  registerArray : any = {}
  config = {
    allowNumbersOnly: true,
    length: 6,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      width: '50px',
      height: '50px',
    },
  };
  reCaptchaVerifier!: any;

  constructor(private toastr: ToastrService, private router: Router, private ngZone: NgZone, private auth: AuthService) {

  }

  registerform=new FormGroup({
    Name: new FormControl("",[Validators.required, Validators.minLength(3),Validators.pattern("[a-zA-Z][a-zA-Z ]*")]),
   
    Email: new FormControl("",[Validators.required,Validators.email]),
 
    Mobile: new FormControl("", [Validators.required])
}); 


  ngOnInit(): void {

  }

  register() {
    // if (this.email == '') {
    //   alert("you have to enter your email")
    //   return;
    // }

    // if (this.password == '') {
    //   alert("you have to enter your password")
    //   return;
    // }

    this.email = this.Email.value;
    this.name = this.Name.value;
    this.password = this.registerArray.Password;

    console.log('name is ', this.name);
    
    console.log('email is ', this.email);

    console.log('password is ', this.registerArray.Password);

    console.log('phone number is ', this.phoneNumber);
    
    
    console.log('ye array is ', this.registerArray);
  

    this.auth.getUserByEmail(this.email).subscribe((data: any) => {
      if (data != null) {
        alert('User already registered');
      }
      else {
        this.auth.register(this.name, this.email, this.password, this.phoneNumber).subscribe((data: any) => {
          console.log(data);
        });
        this.email = '';
        this.password = '';
        this.toastr.success('User registered successfully', 'Success')
      }
    })

  }

  getOTP() {
    this.reCaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      'sign-in-button', {
        size: 'invisible',
      }
    );
    console.log(this.reCaptchaVerifier);

    console.log(this.phoneNumber);
    firebase
      .auth()
      .signInWithPhoneNumber(this.phoneNumber, this.reCaptchaVerifier)
      .then((confirmationResult) => {
        localStorage.setItem(
          'verificationId',
          JSON.stringify(confirmationResult.verificationId)
        );
        this.ngZone.run(() => {
          jQuery('#otpModal').modal('show');
        });
      })
      .catch((error) => {
        console.log(error.message);
        alert(error.message);
        setTimeout(() => {
          window.location.reload();
        }, 5000);
      });
  }

  onOtpChange(otp: string) {
    this.otp = otp;
  }

  handleClick() {
    console.log(this.otp);
    this.verify = JSON.parse(localStorage.getItem('verificationId') || '{}');
    console.log(this.verify);
    var credential = firebase.auth.PhoneAuthProvider.credential(
      this.verify,
      this.otp
    );

    console.log(credential);
    firebase
      .auth()
      .signInWithCredential(credential)
      .then((response: any) => {
        console.log(response);
        localStorage.setItem('user_data', JSON.stringify(response));
        this.ngZone.run(() => {
          jQuery('#otpModal').modal('hide');
          console.log('verified');
        });
      })
      .catch((error: { message: any; }) => {
        console.log(error);
        alert(error.message);
      });
  }
  get Email(): FormControl{
    return this.registerform.get("Email") as FormControl;
  }
  get Name(): FormControl{
    return this.registerform.get("Name") as FormControl;
  }
  get Mobile() : FormControl{  
    return this.registerform.get("Mobile") as FormControl; 
  }  

}
