import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  user: any;
  photoURL = "";
  photoFound = false;

  constructor(private service: AuthService) {
    this.user = JSON.parse(localStorage.getItem('user') || "");
    this.photoURL = localStorage.getItem('photoURL') || "";
    if (this.photoURL.length > 0) {
      this.photoURL = this.photoURL.substring(1, this.photoURL.length-1);
      this.photoFound = true;
    }
  }

  signOut() {
    this.service.logout();
  }




}




