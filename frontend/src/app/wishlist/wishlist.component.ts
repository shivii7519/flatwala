import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RentserviceService } from '../rentservice.service';
import { WishlistService } from '../wishlist.service';

declare var jQuery: any;
@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  apartments: any[];
  owner: any;
  user: any;

  constructor(private service: RentserviceService, private wishService: WishlistService, private router: Router) {
    this.apartments = [];
  };

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user') || "");
    this.wishService.getWishlistByUserId(this.user.id).subscribe((data: any) => {
      data.forEach((obj: any) => {
        this.apartments.push(obj.flat);
      });
      console.log(this.apartments);
    });
  }

  ownerDetails(flat: any) {
    jQuery('#ownerModal').modal('show');
  }

  deletewishlist(flatId: any) {
    this.wishService.deleteWishlist(this.user.id, flatId).subscribe((data) => {
      this.apartments = this.apartments.filter(flat => flat.flatId != flatId);
      console.log(data, "flat removed");
    })
  }

  getPhoneNo() {
    jQuery('#phoneModal').modal('show');
  }

  getFlatDetails(flatId: any) {
    localStorage.setItem('selectedFlat', flatId);
    this.router.navigate(['../flatDetails']);
  }
}
