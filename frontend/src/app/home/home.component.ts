import { Component, OnInit } from '@angular/core';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: any;

  constructor(private auth: AuthService, private router: Router, private authService: SocialAuthService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user') || "");
  }

  logout() {
    this.auth.logout();
  }

}
