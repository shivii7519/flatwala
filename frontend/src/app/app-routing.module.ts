import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlatdetailsComponent } from './flatdetails/flatdetails.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MylistingComponent } from './mylisting/mylisting.component';
import { RegisterComponent } from './register/register.component';
import { RentpageComponent } from './rentpage/rentpage.component';
import { SearchFlatComponent } from './search-flat/search-flat.component';
import { SellregComponent } from './sellreg/sellreg.component';
import { VerifyemailComponent } from './verifyemail/verifyemail.component';
import { WishlistComponent } from './wishlist/wishlist.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'verify', component: VerifyemailComponent },
  { path: 'home', component: HomeComponent },
  { path: 'forget', component: ForgetpasswordComponent },
  { path: 'registerFlat', component: SellregComponent },
  { path: 'searchFlat', component: SearchFlatComponent },
  { path: 'availableFlats', component: RentpageComponent },
  { path: 'wishlist', component: WishlistComponent },
  { path: 'flatDetails', component: FlatdetailsComponent},
  { path: 'myListing', component: MylistingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
