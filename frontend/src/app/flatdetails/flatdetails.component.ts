import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RentserviceService } from '../rentservice.service';
import { WishlistService } from '../wishlist.service';

declare var jQuery: any;

@Component({
  selector: 'app-flatdetails',
  templateUrl: './flatdetails.component.html',
  styleUrls: ['./flatdetails.component.css']
})
export class FlatdetailsComponent implements OnInit {
  user: any;
  flat_details: any;
  flat_id: any;
  selectedCity = '';
  selectedBHKs = '';
  selectedFlat: any;
  selectedFlatId: any;

  constructor(private service: RentserviceService, private wishlistService: WishlistService, private toaster: ToastrService) {
    this.user = JSON.parse(localStorage.getItem('user') || "");
    this.selectedCity = localStorage.getItem('selectedCity') || "";
    this.selectedBHKs = localStorage.getItem('selectedBHK') || "";
    this.selectedFlatId = (localStorage.getItem('selectedFlat')) || 0;
  }

  ngOnInit(): void {
    this.service.getFlatByID(this.selectedFlatId).subscribe((data: any) => { this.flat_details = data 
    console.log(data) 
    console.log(data.maplink)})
  }

  ownerDetails() {
    jQuery('#ownerModal').modal('show');
  }

  addToWishlist(flatId: any) {
    this.wishlistService.checkWishlist(this.user.id, flatId).subscribe((data: any) => {
      if (data.length == 0) {
        this.wishlistService.addToWishList(flatId, this.user.id).subscribe((data: any) => {
          console.log("flat added to wishlist");
          this.toaster.success('Flat added to wishlist', 'Success');
        })
      }
      else {
        console.log("flat is already registered");
        this.toaster.warning('Flat already added to wishlist', 'Warning');
      }
    })
  }

}
