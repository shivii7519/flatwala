import { Component, OnInit } from '@angular/core';
import { MyListingService } from '../my-listing.service';

@Component({
  selector: 'app-mylisting',
  templateUrl: './mylisting.component.html',
  styleUrls: ['./mylisting.component.css']
})
export class MylistingComponent implements OnInit{
  user: any;
  uploadedFlats:any=[];

  constructor(private service: MyListingService){
    this.user = JSON.parse(localStorage.getItem('user') || "");
  }

  ngOnInit(): void{
    this.getListedFlats();
  }

  getListedFlats() {
    this.service.getListedFlats(this.user.id).subscribe((data) => {
      this.uploadedFlats=data;
      console.log("GetListed:", this.uploadedFlats);
    });
  }

  deleteFlat(flatId: any) {
    this.service.deleteMyListingById(this.user.id, flatId).subscribe((data) => {
      console.log('Flat deleted');
      this.getListedFlats();
    })
  }

}
