package com.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.User;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {
	
	@Autowired
	UserDAO userDAO;
	
	@PostMapping("/registerUser")
	public User registerEmployee(@RequestBody User user) {
		return userDAO.registerUser(user);
	}
	
	@GetMapping("/getUser/{emailId}")
	public User getUser(@PathVariable("emailId") String emailId) {
		return userDAO.getUser(emailId);
	}
}
