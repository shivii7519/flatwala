package com.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.FlatDAO;
import com.model.Flat;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class FlatController {

	@Autowired
	FlatDAO flatDAO;

	@PostMapping("/registerFlat")
	public Flat registerFlat(@RequestBody Flat flat) {
		return flatDAO.registerFlat(flat);
		
	}
	
	@RequestMapping("/getflatById/{flat_id}")
	public Flat getFlatById(@PathVariable("flat_id") int flat_id) {
		Flat details = flatDAO.getFlatById(flat_id);
		if (details != null)
			return details;

		return null;
	}
	
	@GetMapping("/getAllflats")
	public List<Flat> getAllflats() {
		return flatDAO.getAllflats();
	}
	
	@GetMapping("/getFlatsByCity/{city}/{bhk}")
	public List<Flat> getFlatsByCity(@PathVariable("city") String city, @PathVariable("bhk") String bhk) {
		List<Flat> flats = flatDAO.getFlatsByCity(city, bhk);
		
		if (flats != null)
			return flats;
		return null;
	}
}
