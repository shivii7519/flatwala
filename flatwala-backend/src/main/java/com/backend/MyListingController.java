package com.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MyListingDAO;
import com.model.MyListing;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MyListingController {
	@Autowired
	MyListingDAO myListingDAO;
	
	@PostMapping("/addToMyListing")
	public MyListing addToMyListing(@RequestBody MyListing myListing) {
		return myListingDAO.addToMyListing(myListing);
	}
	
	@GetMapping("/getMyListing/{userId}")
	public List<MyListing> getMyListing(@PathVariable("userId") int userId) {
		return myListingDAO.getMyListing(userId);
	}
	
	@GetMapping("/getAllMyListings")
	public List<MyListing> getAllMyListings() {
		return myListingDAO.getAllListing();
	}
	
	@DeleteMapping("deleteMyListing/{userId}/{flatId}")
	public int deleteMyListingById(@PathVariable("userId") int userId, @PathVariable("flatId") int flatId) {
		return myListingDAO.deleteMyListingById(userId, flatId);
	}
}
