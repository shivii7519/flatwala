package com.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.WishlistDAO;
import com.model.Wishlist;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class WishlistController {
	@Autowired
	WishlistDAO wishlistDAO;
	
	@PostMapping("/addToWishlist")
	public Wishlist addToWishlist(@RequestBody Wishlist wishlist) {
		return wishlistDAO.addToWishlist(wishlist);
	}
	
	@GetMapping("/getWishlist/{userId}")
	public List<Wishlist> getWishlist(@PathVariable("userId") int userId) {
		return wishlistDAO.getWishlist(userId);
	}
	
	@GetMapping("/getAllWishlists")
	public List<Wishlist> getAllWishlists() {
		return wishlistDAO.getAllWishList();
	}
	
	@GetMapping("/checkWishlist/{userId}/{flatId}")
	public List<Wishlist> checkWishlist(@PathVariable("userId") int userId,@PathVariable("flatId") int flatId){
		return wishlistDAO.checkWishlist(userId,flatId);
		
	}
	
	@DeleteMapping("/deleteWishlist/{userId}/{flatId}")
	public int deleteWishlist(@PathVariable("userId") int userId,@PathVariable("flatId") int flatId){
		return wishlistDAO.deleteWishlist(userId,flatId);
		
	}
	
	
		
	
	
}
