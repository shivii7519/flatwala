package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Wishlist;

@Service @Transactional
public class WishlistDAO {
	
	@Autowired
	WishlistRepository wishlistRepo;
	
	public List<Wishlist> getWishlist(int userId) {
		return wishlistRepo.getWishlist(userId);
	}
	
	public Wishlist addToWishlist(Wishlist wishlist) {
		return wishlistRepo.save(wishlist);
	}
	
	public List<Wishlist> getAllWishList() {
		return wishlistRepo.findAll();
	}

	public List<Wishlist> checkWishlist(int userId, int flatId) {
		return wishlistRepo.checkWishlist(userId,flatId);
	}

	public int deleteWishlist(int userId, int flatId) {
		return wishlistRepo.deleteWishlist(userId,flatId);
	}

}
