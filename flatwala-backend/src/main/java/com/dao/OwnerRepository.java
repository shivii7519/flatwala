package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Owner;

public interface OwnerRepository extends JpaRepository<Owner, Integer>{

//	@Query("from owner d where d.ownerName = :ownerName")
//	Owner findByName(@Param("ownerName") String ownerName);
}
