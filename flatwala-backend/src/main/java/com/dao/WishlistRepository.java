package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Wishlist;

public interface WishlistRepository extends JpaRepository<Wishlist, Integer>{
	
	@Query(value="SELECT * FROM Wishlist WHERE Wishlist.id = :userId", nativeQuery=true)
	List<Wishlist> getWishlist(@Param("userId") int userId);				
	
	@Query(value="SELECT * FROM Wishlist WHERE Wishlist.id = :userId and Wishlist.flat_id = :flatId", nativeQuery=true)
	List<Wishlist> checkWishlist(@Param("userId") int userId, @Param("flatId") int flatId);
	
	@Modifying
	@Query(value="DELETE FROM Wishlist WHERE Wishlist.id = :userId and Wishlist.flat_id = :flatId", nativeQuery=true)
	int deleteWishlist(@Param("userId") int userId, @Param("flatId") int flatId);
	
}
