package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Flat;

@Service
public class FlatDAO {
	@Autowired
	FlatRepository flatRepository;
	
	public Flat registerFlat(Flat flat) {
		return flatRepository.save(flat);
	}
	
	public Flat getFlatById(int flat_id) {
		return flatRepository.findById(flat_id);
	}
	
	public List<Flat> getAllflats() {
		return flatRepository.findAll();
	}
	
	public List<Flat> getFlatsByCity(String city, String bhk) {
		return flatRepository.findByCity(city, bhk);
	}
	
}
