package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.model.Rent;

@Service
public class RentDAO {
	@Autowired
	RentRepository rentrepository;

	public List<Rent> getAllflats() {
		System.out.println(rentrepository.findAll());
		return rentrepository.findAll();
	}
	
	public List<Rent> getFlatsByCity(String city, String bhk) {
		return rentrepository.findByCity(city, bhk);
	}
//	public Rent getFlatsByCity(String city) {
//		return rentrepository.findByCity();
//	}
	
	public Rent enterFlat(Rent rent){
		return rentrepository.save(rent);
	}

}
