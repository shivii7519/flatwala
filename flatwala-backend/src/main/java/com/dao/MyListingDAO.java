package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.MyListing;

@Service @Transactional
public class MyListingDAO {
	@Autowired
	MyListingRepository myListingRepo;
	
	public List<MyListing> getAllListing() {
		return myListingRepo.findAll();
	}
	
	public MyListing addToMyListing(MyListing myListing) {
		return myListingRepo.save(myListing);
	}
	
	public List<MyListing> getMyListing(int userId) {
		return myListingRepo.getMyListing(userId);
	}
	
	public int deleteMyListingById(int userId, int flatId) {
		return myListingRepo.deleteMyListingById(userId, flatId);
	}
}
