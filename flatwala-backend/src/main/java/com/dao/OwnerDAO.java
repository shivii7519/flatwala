package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Owner;
import com.model.Rent;

@Service
public class OwnerDAO {
	@Autowired
	OwnerRepository ownerepository;
	
	public Owner getOwnerbyid(int ownerid) {
		return ownerepository.findById(ownerid).orElse(null);
	}

//	public List<Owner> getOwnerbyid() {
//		return ownerepository.findAll();
//	}

}
