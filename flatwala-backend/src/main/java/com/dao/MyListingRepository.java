package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.MyListing;

public interface MyListingRepository extends JpaRepository<MyListing, Integer>{
	
	@Query(value="SELECT * FROM My_Listing WHERE My_Listing.id = :userId", nativeQuery=true)
	List<MyListing> getMyListing(@Param("userId") int userId);

	@Modifying
	@Query(value="DELETE FROM My_Listing WHERE My_Listing.id = :userId and My_Listing.flat_id = :flatId", nativeQuery=true)
	int deleteMyListingById(@Param("userId") int userId, @Param("flatId")int flatId);
	
}
