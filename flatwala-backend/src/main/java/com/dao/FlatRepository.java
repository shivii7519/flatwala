package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Flat;

public interface FlatRepository extends JpaRepository<Flat, Integer>{
	@Query("from Flat f where f.city = :city and f.bHK = :bhk")
	List<Flat> findByCity(@Param("city") String city, @Param("bhk") String bhk);
	
	@Query("from Flat f where f.flatId = :flat_id")
	Flat findById(@Param("flat_id") int flat_id);
}
