package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Owner;
import com.model.Rent;


public interface RentRepository extends JpaRepository<Rent, Integer> {

	@Query("from Rent u where u.city = :city and u.bhk = :bhk")
	List<Rent> findByCity(@Param("city") String city, @Param("bhk") String bhk);
	
//	@Query(value="SELECT state, admission_number FROM state", nativeQuery=true)
//	@Query("from User u where u.email = :email")
}