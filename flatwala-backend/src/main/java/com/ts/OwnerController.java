package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dao.OwnerDAO;
import com.model.Owner;

@RestController
public class OwnerController {
	@Autowired
	OwnerDAO ownerDAO;
	
	@GetMapping("/getOwnerbyid/{ownerid}")
	public Owner getOwnerById(@PathVariable("ownerid") int ownerid) {
		Owner owner = ownerDAO.getOwnerbyid(ownerid);
		
		if (owner != null)
			return owner;
		return new Owner(0, "Department Not Found!!!", "", "", "", "", "", null);
	}

//	@GetMapping("/getOwner")
//	public Owner getOwner(){
//		
//	}
}
	
