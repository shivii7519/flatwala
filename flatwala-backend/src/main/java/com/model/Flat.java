package com.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Flat {
	@Id
	@GeneratedValue
	private int flatId;
	private String ownerType;
	private String ownerName;
	private String ownerPhone;
	private String ownerMail;
	private String flatName;
	private String city;
	private String locality;
	private String bHK;
	private int rent;
	private String floorNo;
	private String furnishedStatus;
	private String bathroom;
	private String maplink;
	
	@JsonIgnore
	@OneToMany(mappedBy="flat", fetch = FetchType.EAGER)
	private Set<Wishlist> wishlist = new HashSet<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="flat", fetch = FetchType.EAGER)
	private Set<MyListing> myList = new HashSet<>();

	public Flat() {
		super();
	}

	public Flat(int flatId, String ownerType, String ownerName, String ownerPhone, String ownerMail, String flatName,
			String city, String locality, String bHK, int rent, String floorNo, String furnishedStatus, String bathroom,
			Set<Wishlist> wishlist, Set<MyListing> myList) {
		super();
		this.flatId = flatId;
		this.ownerType = ownerType;
		this.ownerName = ownerName;
		this.ownerPhone = ownerPhone;
		this.ownerMail = ownerMail;
		this.flatName = flatName;
		this.city = city;
		this.locality = locality;
		this.bHK = bHK;
		this.rent = rent;
		this.floorNo = floorNo;
		this.furnishedStatus = furnishedStatus;
		this.bathroom = bathroom;
		this.wishlist = wishlist;
		this.myList = myList;
	}

	public int getFlatId() {
		return flatId;
	}

	public void setFlatId(int flatId) {
		this.flatId = flatId;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerPhone() {
		return ownerPhone;
	}

	public void setOwnerPhone(String ownerPhone) {
		this.ownerPhone = ownerPhone;
	}

	public String getOwnerMail() {
		return ownerMail;
	}

	public void setOwnerMail(String ownerMail) {
		this.ownerMail = ownerMail;
	}

	public String getFlatName() {
		return flatName;
	}

	public void setFlatName(String flatName) {
		this.flatName = flatName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getbHK() {
		return bHK;
	}

	public void setbHK(String bHK) {
		this.bHK = bHK;
	}

	public int getRent() {
		return rent;
	}

	public void setRent(int rent) {
		this.rent = rent;
	}

	public String getFloorNo() {
		return floorNo;
	}

	public void setFloorNo(String floorNo) {
		this.floorNo = floorNo;
	}

	public String getFurnishedStatus() {
		return furnishedStatus;
	}

	public void setFurnishedStatus(String furnishedStatus) {
		this.furnishedStatus = furnishedStatus;
	}

	public String getBathroom() {
		return bathroom;
	}

	public void setBathroom(String bathroom) {
		this.bathroom = bathroom;
	}
	
	public String getmaplink() {
		return maplink;
	}

	public void setmaplink(String maplink) {
		this.maplink = maplink;
	}

	public Set<Wishlist> getWishlist() {
		return wishlist;
	}

	public void setWishlist(Set<Wishlist> wishlist) {
		this.wishlist = wishlist;
	}

	public Set<MyListing> getMyList() {
		return myList;
	}

	public void setMyList(Set<MyListing> myList) {
		this.myList = myList;
	}

	@Override
	public String toString() {
		return "Flat [flatId=" + flatId + ", ownerType=" + ownerType + ", ownerName=" + ownerName + ", ownerPhone="
				+ ownerPhone + ", ownerMail=" + ownerMail + ", flatName=" + flatName + ", city=" + city + ", locality="
				+ locality + ", bHK=" + bHK + ", rent=" + rent + ", floorNo=" + floorNo + ", furnishedStatus="
				+ furnishedStatus + ", bathroom=" + bathroom + ", wishlist=" + wishlist + ", myList=" + myList + "]";
	}

	
	
	
}
