package com.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Wishlist {
	@Id@GeneratedValue
	private int wishlistId;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id")   // here id is user id
	User user;
	
	@ManyToOne
	@JoinColumn(name="flatId")
	Flat flat;

	public Wishlist() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Wishlist(int wishlistId, User user, Flat flat) {
		super();
		this.wishlistId = wishlistId;
		this.user = user;
		this.flat = flat;
	}

	public int getWishlistId() {
		return wishlistId;
	}

	public void setWishlistId(int wishlistId) {
		this.wishlistId = wishlistId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Flat getFlat() {
		return flat;
	}

	public void setFlat(Flat flat) {
		this.flat = flat;
	}

	@Override
	public String toString() {
		return "Wishlist [wishlistId=" + wishlistId + ", user=" + user + ", flat=" + flat + "]";
	}
	
	
}
