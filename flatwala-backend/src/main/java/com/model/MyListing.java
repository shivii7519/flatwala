package com.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MyListing {
	@Id@GeneratedValue
	private int listingId;
	
	@ManyToOne (cascade = CascadeType.PERSIST)
	@JoinColumn(name="id")   // here id is user id
	User user;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="flatId")
	Flat flat;

	public MyListing() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MyListing(int listingId, User user, Flat flat) {
		super();
		this.listingId = listingId;
		this.user = user;
		this.flat = flat;
	}

	public int getListingId() {
		return listingId;
	}

	public void setListingId(int listingId) {
		this.listingId = listingId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Flat getFlat() {
		return flat;
	}

	public void setFlat(Flat flat) {
		this.flat = flat;
	}

	@Override
	public String toString() {
		return "MyListing [listingId=" + listingId + ", user=" + user + ", flat=" + flat + "]";
	}
	
	
}
