package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Owner {
	@Id@GeneratedValue
	private int ownerid;
	private String ownerName;
	private String occupation;
	private String mob;
	private String email;
	private String add1;
	private String add2;
	private String add3;
	
	@JsonIgnore
	@OneToMany(mappedBy="owner")
	List<Rent> flatRent = new ArrayList<Rent>();

	public Owner() {
		super();
	}

	public Owner(int ownerid, String ownerName, String occupation, String mob, String email, String add1, String add2,
			String add3) {
		super();
		this.ownerid = ownerid;
		this.ownerName = ownerName;
		this.occupation = occupation;
		this.mob = mob;
		this.email = email;
		this.add1 = add1;
		this.add2 = add2;
		this.add3 = add3;
	}

	public int getOwnerid() {
		return ownerid;
	}

	public void setOwnerid(int ownerid) {
		this.ownerid = ownerid;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getMob() {
		return mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdd1() {
		return add1;
	}

	public void setAdd1(String add1) {
		this.add1 = add1;
	}

	public String getAdd2() {
		return add2;
	}

	public void setAdd2(String add2) {
		this.add2 = add2;
	}

	public String getAdd3() {
		return add3;
	}

	public void setAdd3(String add3) {
		this.add3 = add3;
	}

	public List<Rent> getFlatRent() {
		return flatRent;
	}

	public void setFlatRent(List<Rent> flatRent) {
		this.flatRent = flatRent;
	}

//	@Override
//	public String toString() {
//		return "Owner [ownerid=" + ownerid + ", ownerName=" + ownerName + ", occupation=" + occupation + ", mob=" + mob
//				+ ", email=" + email + ", add1=" + add1 + ", add2=" + add2 + ", add3=" + add3 + ", flatRent=" + flatRent
//				+ "]";
//	}

	
	
}