package com.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {
	@Id
	@GeneratedValue
	private int id;
	private String name;
	private String email;
//	private String password;
	private String phone;
	
	@JsonIgnore
	@OneToMany(mappedBy="user", fetch = FetchType.EAGER)
	private Set<Wishlist> wishlist = new HashSet<>();
	
//	@JsonIgnore
//	@OneToMany(mappedBy="user", fetch = FetchType.EAGER)
//	private List<MyListing> myListing = new ArrayList<>();

	public User() {
		super();
	}

	public User(int id, String name, String email, String phone) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
//		this.password = password;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone
				+ "]";
	}
}
