package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Rent {
	@Id@GeneratedValue
	private int flatId;
	private String flatName;
	private String bhk;
	private String area;
	private String price;
	private String deposit;
	private String city;
		
	@ManyToOne
	@JoinColumn(name = "ownerid")
	Owner owner;

	public Rent() {
		super();
	}

	public Rent(int flatId, String flatName, String bhk, String area, String price, String deposit, String city,
			Owner owner) {
		super();
		this.flatId = flatId;
		this.flatName = flatName;
		this.bhk = bhk;
		this.area = area;
		this.price = price;
		this.deposit = deposit;
		this.city = city;
		this.owner = owner;
	}

	public int getFlatId() {
		return flatId;
	}

	public void setFlatId(int flatId) {
		this.flatId = flatId;
	}

	public String getFlatName() {
		return flatName;
	}

	public void setFlatName(String flatName) {
		this.flatName = flatName;
	}

	public String getBhk() {
		return bhk;
	}

	public void setBhk(String bhk) {
		this.bhk = bhk;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Rent [flatId=" + flatId + ", flatName=" + flatName + ", bhk=" + bhk + ", area=" + area + ", price="
				+ price + ", deposit=" + deposit + ", city=" + city + ", owner=" + owner + "]";
	}
	
	
}